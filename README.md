# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://mircheska@bitbucket.org/mircheska/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/mircheska/stroboskop/commits/cd9924edf5f7e4fa333de31453353fc253e545e6

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mircheska/stroboskop/commits/813d2de7217bfff0641ebf0516e902612f3f17e0

Naloga 6.3.2:
https://bitbucket.org/mircheska/stroboskop/commits/b6d22b7ab1799da9645860fcca3c6aed5c993839

Naloga 6.3.3:
https://bitbucket.org/mircheska/stroboskop/commits/d925a9482d0d399a04ef6e8bd918f15e45c861c2

Naloga 6.3.4:
https://bitbucket.org/mircheska/stroboskop/commits/8e1f562f5beeb7225dcaea1c59fcac6b454891f4

Naloga 6.3.5:

```
git merge master izgled 
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mircheska/stroboskop/commits/276c6b9487464725cbd1cb7e3f0e448d58ccf91a

Naloga 6.4.2:
https://bitbucket.org/mircheska/stroboskop/commits/83eee6b65bfc0c42d3cc9a6ebd5102198c5f9700

Naloga 6.4.3:
https://bitbucket.org/mircheska/stroboskop/commits/f991c5724ec07b153812fc08c9ab1f80776d95b8

Naloga 6.4.4:
https://bitbucket.org/mircheska/stroboskop/commits/b1f0c6e4313100a5febde9a101b762eaa0c8c4b2